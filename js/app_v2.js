var myFeatureTable;
require([
    "esri/layers/FeatureLayer",
    "esri/dijit/FeatureTable",
    "esri/geometry/Extent",
    "esri/graphicsUtils",
    "esri/tasks/query",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/map",
    "dojo/dom",
    "dojo/parser",
    "dojo/ready",
    "dojo/on",
    "dijit/layout/ContentPane",
    "dijit/layout/BorderContainer",
    "esri/Color",
    "esri/dijit/Scalebar",
     "esri/toolbars/navigation",
    "esri/geometry/webMercatorUtils",
    "dojo/_base/lang",
    "esri/dijit/InfoWindowLite",
    "esri/InfoTemplate",
    "dojo/dom-construct",
    "dojo/_base/connect",
    "esri/domUtils",
    "dijit/registry",
    "esri/dijit/Popup",
    "esri/dijit/PopupTemplate",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "dojo/dom-class",
    "esri/dijit/HomeButton",
    "esri/arcgis/utils",
    "esri/dijit/LayerList",
    "esri/request",
    "esri/geometry/Point",
    "esri/graphic",
    "esri/tasks/BufferParameters",
    "esri/tasks/GeometryService",
    "dijit/form/RadioButton",
    "dijit/form/Button",
    "dojo/request",
    "esri/dijit/Legend",
    "esri/renderers/ClassBreaksRenderer",
    "dojo/fx/Toggler",
    "dojo/fx",
    "dojo/dom-geometry",
    "esri/dijit/BasemapGallery",
    "esri/dijit/OverviewMap",
], function (
    FeatureLayer, FeatureTable, Extent, graphicsUtils, Query, SimpleMarkerSymbol,SimpleLineSymbol, Map,
    dom, parser, ready, on, ContentPane, BorderContainer ,Color, Scalebar, Navigation ,webMercatorUtils, lang, InfoWindowLite, InfoTemplate, domConstruct,connect,domUtils,registry,Popup,PopupTemplate, SimpleFillSymbol, Color,
    domClass,HomeButton,arcgisUtils,LayerList,esriRequest,Point,Graphic,BufferParameters,GeometryService,RadioButton,Button,request,Legend,ClassBreaksRenderer,Toggler,coreFx,domGeom,BasemapGallery,OverviewMap
) {

    //parser.parse();
    esriConfig.defaults.io.corsEnabledServers.push('service.iris.edu');
    //esriConfig.defaults.io.proxyUrl =
    var url="";
    var UltimoSismo="";
    ready(function(){

        on(dom.byId("previous"), "click", selectPrevious);
        on(dom.byId("next"), "click", selectNext);
            //parser.parse();
        var fill = new SimpleFillSymbol("solid", null, new Color("#A4CE67"));

        var popup = new Popup({
            fillSymbol: fill,
            titleInBody: false
        }, domConstruct.create("div"));
        //Add the dark theme which is customized further in the <style> tag at the top of this page
        domClass.add(popup.domNode, "dark");

        var map = new Map("map",{
            basemap: "topo",
            center: [-72, 4],
            zoom: 6,
            infoWindow: popup
        });

        var globals;
        globals = new OverviewMap({
            map: map,
            visible: true
        }, dom.byId('overviewMapDiv'));
        globals.startup();

        var basemapGallery = new BasemapGallery({
            showArcGISBasemaps: true,
            map: map
        }, "basemapGallery");
        basemapGallery.startup();

        basemapGallery.on("error", function(msg) {
            console.log("basemap gallery error:  ", msg);
        });



        
        var home = new HomeButton({
            map: map
        }, "HomeButton");
        home.startup();

        var scalebar = new Scalebar({
            map: map,
            // "dual" displays both miles and kilometers
            // "english" is the default, which displays miles
            // use "metric" for kilometers
            scalebarUnit: "dual"
        });

        sumaFecha = function(d, fecha)
        {
            var Fecha = new Date();
            var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
            var sep =  '-';
            var aFecha = sFecha.split(sep);
            var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
            fecha= new Date();
            fecha.setDate(fecha.getDate()+parseInt(d));
            var anno=fecha.getFullYear();
            var mes= fecha.getMonth()+1;
            var dia= fecha.getDate();
            var hora= fecha.getHours();
            var minuto = fecha.getMinutes();
            var segundo = fecha.getSeconds();
            mes = (mes < 10) ? ("0" + mes) : mes;
            dia = (dia < 10) ? ("0" + dia) : dia;
            hora=(hora<10) ? ("0" + hora) : hora;
            minuto=(minuto<10) ? ("0" + minuto) : minuto;
            segundo=(segundo<10) ? ("0" + segundo) : segundo;
            var fechaFinal = anno+sep+mes+sep+dia+"T"+hora+":"+minuto+":"+segundo;
            return (fechaFinal);
        };

        //Load a FeatureTable to the application once map loads





        var urlTemp= "http://service.iris.edu/fdsnws/event/1/query?&format=text&nodata=404&orderby=time";
        var url=urlTemp +"&starttime="+sumaFecha(-1)+"&endtime="+sumaFecha(0);
        console.log(url);
        map.on("load", loadTable);

        var legend = new Legend({
            map: map
        }, "legendDiv");
        legend.startup();

        function loadTable(){


            //var censusUrl = "http://srvags.sgc.gov.co/arcgis/rest/services/METG/METADATO/MapServer/0";

            on(dom.byId("Consulta"), "click", function(evt){
                // Request the text file

                var urlconsulta="";
                var check1= dom.byId("consulta1");
                var check2=dom.byId("consulta2");
                var check3=dom.byId("consulta3");

                if (check1.checked)
                {
                    urlconsulta=urlTemp +"&starttime="+sumaFecha(-1)+"&endtime="+sumaFecha(0);
                    url=urlconsulta;
                    requestPhotos();
                }
                if (check2.checked)
                {
                    urlconsulta=urlTemp +"&starttime="+sumaFecha(-30)+"&endtime="+sumaFecha(0);
                    url=urlconsulta;
                    requestPhotos();
                }if (check3.checked)
                {
                    urlconsulta=urlTemp +"&starttime="+sumaFecha(-60)+"&endtime="+sumaFecha(0);
                    url=urlconsulta;
                    requestPhotos();
                }
                
                //alert(urlconsulta);

            });

            console.log("Url: "+ url);
            gsvc = new esri.tasks.GeometryService("https://utility.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");

            var template = new PopupTemplate({
                title: "{EventLocationName}",
                description: "Magnitud: {Magnitude}  <br> Profundidad: {Depthkm} " +
                " <br> Fecha: {Time}  <br> <a href='prueba'>Pagina Detalle</a>",
                fieldInfos: [{
                    fieldName: "EventID",
                    alias:"Identificador"
                }, {
                    fieldName: "EventLocationName",
                    alias:"Localización"
                }, 
                    {
                    fieldName: "Magnitude",
                    alias:"Magnitud"
                },
                    {
                        fieldName: "Author",
                        alias: "Autor"

                    }]

            });

            var featureCollection = {
                "layerDefinition": null,
                "featureSet": {
                    "features": [],
                    "geometryType": "esriGeometryPoint"
                }
            };
            featureCollection.layerDefinition = {
                "name": "Eventos Sísmicos",
                "geometryType": "esriGeometryPoint",
                "objectIdField": "ObjectID",
                "defaultVisibility": true,

                "fields": [{
                    "name": "ObjectID",
                    "alias": "ObjectID",
                    "type": "esriFieldTypeOID"
                }, {
                    "name": "EventID",
                    "alias": "Identificador",
                    "type": "esriFieldTypeString"
                }, {
                    "name": "Time",
                    "alias": "Tiempo",
                    "type": "esriFieldTypeString"
                },
                    {
                        "name": "Depthkm",
                        "alias": "Profundidad",
                        "type": "esriFieldTypeString"
                    },
                    {
                        "name": "Author",
                        "alias": "Autor",
                        "type": "esriFieldTypeString"
                    },
                    {
                        "name": "Magnitude",
                        "type": "esriFieldTypeDouble",
                        "alias": "Magnitud",
                    },
                    {
                        "name": "EventLocationName",
                        "type": "esriFieldTypeString",
                        "alias": "Localización",
                    }
                ]
            };

            var myFeatureLayer = new FeatureLayer(featureCollection, {
                id: 'EventosSismicos',
                infoTemplate: template
            });

            var rendererField = "Magnitude";

            var renderer = new ClassBreaksRenderer();
            renderer.attributeField = rendererField;

            var markerSymbol = new SimpleMarkerSymbol();
            markerSymbol.setColor(new Color([202, 200, 200, 1]));
            markerSymbol.setSize(4);
            markerSymbol.outline.setColor(new Color([202, 200, 200, 1]));
            markerSymbol.outline.setWidth(1);

            var markerSymbol2= new SimpleMarkerSymbol();
            markerSymbol2.setColor(new Color([202, 200, 200, 1]));
            markerSymbol2.setSize(6);
            markerSymbol2.outline.setColor(new Color([202, 200, 200, 1]));
            markerSymbol2.outline.setWidth(1);

            var markerSymbol3= new SimpleMarkerSymbol();
            markerSymbol3.setColor(new Color([76, 123, 253, 1]));
            markerSymbol3.setSize(8);
            markerSymbol3.outline.setColor(new Color([76, 123, 253, 1]));
            markerSymbol3.outline.setWidth(1);

            var markerSymbol4= new SimpleMarkerSymbol();
            markerSymbol4.setColor(new Color([76, 123, 253, 1]));
            markerSymbol4.setSize(10);
            markerSymbol4.outline.setColor(new Color([76, 123, 253, 1]));
            markerSymbol4.outline.setWidth(1);

            var markerSymbol5= new SimpleMarkerSymbol();
            markerSymbol5.setColor(new Color([34, 166, 76, 1]));
            markerSymbol5.setSize(12);
            markerSymbol5.outline.setColor(new Color([34, 166, 76, 1]));
            markerSymbol5.outline.setWidth(1);

            var markerSymbol6 = new SimpleMarkerSymbol();
            markerSymbol6.setColor(new Color([34, 166, 76, 1]));
            markerSymbol6.setSize(14);
            markerSymbol6.outline.setColor(new Color([34, 166, 76, 1]));
            markerSymbol6.outline.setWidth(1);

            var markerSymbol7= new SimpleMarkerSymbol();
            markerSymbol7.setColor(new Color([243, 250, 40, 1]));
            markerSymbol7.setSize(16);
            markerSymbol7.outline.setColor(new Color([243, 250, 40, 1]));
            markerSymbol7.outline.setWidth(1);

            var markerSymbol8= new SimpleMarkerSymbol();
            markerSymbol8.setColor(new Color([243, 250, 40, 1]));
            markerSymbol8.setSize(18);
            markerSymbol8.outline.setColor(new Color([243, 250, 40, 1]));
            markerSymbol8.outline.setWidth(1);

            var markerSymbol9= new SimpleMarkerSymbol();
            markerSymbol9.setColor(new Color([252, 31, 31, 1]));
            markerSymbol9.setSize(20);
            markerSymbol9.outline.setColor(new Color([252, 31, 31, 1]));
            markerSymbol9.outline.setWidth(1);

            var markerSymbol10= new SimpleMarkerSymbol();
            markerSymbol10.setColor(new Color([252, 31, 31, 1]));
            markerSymbol10.setSize(22);
            markerSymbol10.outline.setColor(new Color([252, 31, 31, 1]));
            markerSymbol10.outline.setWidth(1);

            renderer.addBreak({
                minValue: 0,
                maxValue: 1,
                symbol: markerSymbol
            });
            renderer.addBreak({
                minValue: 1,
                maxValue: 2,
                symbol: markerSymbol2
            });

            renderer.addBreak({
                minValue: 2,
                maxValue: 3,
                symbol: markerSymbol3
            });

            renderer.addBreak({
                minValue: 3,
                maxValue: 4,
                symbol: markerSymbol4
            });

            renderer.addBreak({
                minValue: 4,
                maxValue: 5,
                symbol: markerSymbol5
            });
            renderer.addBreak({
                minValue: 5,
                maxValue: 6,
                symbol: markerSymbol6
            });
            renderer.addBreak({
                minValue: 6,
                maxValue: 7,
                symbol: markerSymbol7
            });

            renderer.addBreak({
                minValue: 7,
                maxValue: 8,
                symbol: markerSymbol8
            });

            renderer.addBreak({
                minValue: 8,
                maxValue: 9,
                symbol: markerSymbol9
            });

            renderer.addBreak({
                minValue: 9,
                maxValue: 10,
                symbol: markerSymbol10
            });

            myFeatureLayer.setRenderer(renderer);
            myFeatureLayer.disableMouseEvents();

            function requestPhotos() {
                //get geotagged photos from flickr
                //tags=flower&tagmode=all
                var requestHandle = esriRequest({
                    url: url,
                    handleAs: "text",
                    //callbackParamName: "jsoncallback"
                });

                requestHandle.then(requestSucceeded, requestFailed);
            }

            function requestSucceeded(response, io) {
                //loop through the items and add to the feature layer

                var features = [];
                var csv= response;
                //var data2 = JSON.parse(data);
                var lines=csv.split("\n");
                var result = [];
                var headers=lines[0].split(" | ");
                for(var i=1;i<lines.length;i++) {

                    var obj = {};
                    var currentline=lines[i].split("|");
                    for(var j=0;j<headers.length;j++) {
                        obj[headers[j].replace(" ","").replace("#","").replace(" ","").replace("/","")] = currentline[j];
                    }
                    var attr = {};
                    attr["EventID"] = obj.EventID;
                    attr["Time"] = obj.Time;
                    attr["Depthkm"] = obj.Depthkm;
                    attr["Author"] = obj.Author;
                    attr["Magnitude"] = parseFloat(obj.Magnitude);
                    attr["EventLocationName"] = obj.EventLocationName;

                    var geometry = new Point(parseFloat(obj.Longitude),parseFloat(obj.Latitude));

                    var graphic = new Graphic(geometry);
                    graphic.setAttributes(attr);
                    features.push(graphic);
                    console.log(obj.Depthkm);
                    if (i==1){
                        UltimoSismo=obj.EventID.toString()+"   "+obj.Time.toString() +"   "+obj.EventLocationName.toString()+"   "+obj.Magnitude.toString();
                        dom.byId("UltimoEvento").innerHTML="<b>ÚLTIMO SISMO </b>"+ "EL DIA. "+obj.Time.toString()+" SE PRESENTO UN SISMO DE MAGNITUD "+ obj.Magnitude.toString() +" EN "+ obj.EventLocationName.toString();
                    }

                }
                myFeatureLayer.clear();
                myFeatureLayer.applyEdits(features, null, null);
                changeHandler();
            }

            function requestFailed(error) {
                console.log('failed');
            }

            //on(myFeatureLayer, "load", function(evt){

                //var extent = myFeatureLayer.fullExtent;
                //if (webMercatorUtils.canProject(extent, map)) {
                    //map.setExtent( webMercatorUtils.project(extent, map) );
            requestPhotos();
            var mapExtentChange = map.on("extent-change", changeHandler);
            function changeHandler(evt){
                var oidFld = myFeatureLayer.objectIdField;
                var query2 = new Query();
                query2.geometry = evt.extent;
                //query2.spatialRelationship = Query.SPATIAL_REL_CONTAINS;
                myFeatureLayer.queryIds(query2, lang.hitch(this, function(objectIds) {
                    myFeatureTable.selectedRowIds = objectIds;
                    myFeatureTable._showSelectedRecords();
                }));
            }
            map.on("layers-add-result", function(results) {

            });

            //set a selection symbol for the featurelayer.
            var symbol = new SimpleMarkerSymbol(
                SimpleMarkerSymbol.STYLE_CIRCLE,
                14,
                new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_NULL,
                    new Color([0, 255, 255, 1]),
                    1
                ),
                new Color([0, 255, 255, 1])
            );
            myFeatureLayer.setSelectionSymbol(symbol);
            map.addLayer(myFeatureLayer);

            map.infoWindow.set("popupWindow", false);
            //initializeSidebar(map);
            //create new FeatureTable and set its properties 
            myFeatureTable = new FeatureTable({
                featureLayer:myFeatureLayer,
                syncSelection:false,
                map : map,
                editable: true,
                dateOptions: {
                    datePattern: 'M/d/y',
                    timeEnabled: true,
                    timePattern: 'H:mm',
                },
                //use fieldInfos object to change field's label (column header), 
                //change the editability of the field, and to format how field values are displayed
                //you will not be able to edit callnumber field in this example. 
                fieldInfos: [{
                    name: "EventID",
                    alias:"Evento",
                    visible:false,
                    
                }, {
                    name:"Time",
                    alias:"Fecha",
                    visible:true,
                }, {
                    name: "Depthkm",
                    alias:"Prof/Km",
                    visible:true,
                    
                },
                    {
                        name: "Magnitude",
                        alias:"Magnitud",
                        visible:true,

                    },
                    {
                        name: "Author",
                        alias:"Autor",
                        visible:false,

                    },
                    {
                        name: "EventLocationName",
                        alias:"Localización",
                        visible:true,

                    }],


                //add custom menu functions to the 'Options' drop-down Menu 
                menuFunctions: [
                    {
                        label: "Zoom To Selected Feature(s)",
                        callback: function(evt){
                            console.log(" -- evt: ", evt);
                            var query = new Query();
                            //selectedRowIds property returns ObjectIds of features selected in the feature table
                            //Use the ObjectIds to query the feature layer. In this case, we will zoom to 
                            //selected features on the map
                            query.objectIds = myFeatureTable.selectedRowIds;
                            query.geometry = map.extent;


                            myFeatureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function(features){
                                //zoom to the selected feature
                                //if only one point feature is selected in the table
                                if (features.length == 1 && features[0].geometry.type ==="point") {
                                    maxZoom = map.getMaxZoom();
                                    map.centerAndZoom(features[0].geometry, maxZoom - 1);

                                }
                                else {
                                    var extent = graphicsUtils.graphicsExtent(features);
                                    map.setExtent(extent)


                                }
                                //show only selected features in the feature table. 
                                myFeatureTable.filterSelectedRecords(true);
                            });
                        }
                    },
                    { label: "Custom Refresh",  callback: customRefresh }
                ]
            }, 'myTableNode');



            myFeatureTable.startup();
            myFeatureTable.on("row-select", function(evt){
                //displayPopupContent(evt.getSelectedFeature());
                var query = new Query();
                var featureLayer = myFeatureTable.featureLayer;
                //console.log("features", evt.selectedRowIds);
                var rowData = myFeatureTable.getRowDataById(myFeatureTable.selectedRowIds[0]);
                query.objectIds = [parseInt(rowData.ObjectID)];
                console.log("row data", rowData.ObjectID);
                featureLayer.selectFeatures(query,FeatureLayer.SELECTION_NEW);


            });

            myFeatureTable.on("refresh", function(evt){

                console.log("refresh event - ", evt);
            });

            myFeatureTable.on("filter", function(evt){
                console.log("filter event - ", evt);
            });
            dojo.connect(map, "onClick", selectFeat);
            function selectFeat(evt){
                myFeatureTable.grid.refresh();
                var centerPoint = new esri.geometry.Point(evt.mapPoint.x,evt.mapPoint.y,evt.mapPoint.spatialReference);
                var mapWidth = map.extent.getWidth();

                //Divide width in map units by width in pixels
                var pixelWidth = mapWidth/map.width;

                //Calculate a 10 pixel envelope width (5 pixel tolerance on each side)
                var tolerance = 10 * pixelWidth;

                //Build tolerance envelope and set it as the query geometry
                var queryExtent = new esri.geometry.Extent(1,1,tolerance,tolerance,evt.mapPoint.spatialReference);

                var query = new Query();
                query.geometry = queryExtent.centerAt(centerPoint);
                console.log("punto - "+"Id:", queryExtent.centerAt(centerPoint));

                var featureLayer = myFeatureTable.featureLayer;
                featureLayer.selectFeatures(query,FeatureLayer.SELECTION_NEW);
                //console.log("punto - "+"Id:", evt);

            }
            function doBuffer(evt) {

                map.graphics.clear();
                var params = new BufferParameters();
                params.geometries  = [ evt.mapPoint ];

                params.distances = [5, 10];
                params.unit = GeometryService.UNIT_KILOMETER;
                params.bufferSpatialReference =  map.spatialReference;
                params.outSpatialReference = map.spatialReference;
                gsvc.buffer(params,selectFeat);
            }

            myFeatureLayer.on("selection-complete", function(evt){
                //myFeatureTable.clearSelection();
                var popup = map.infoWindow;
                displayPopupContent(myFeatureLayer.getSelectedFeatures()[0]);
                var id = myFeatureLayer.getSelectedFeatures()[0].attributes.ObjectID;
                myFeatureTable.grid.select(myFeatureTable.getRowDataById(id));
                console.log("select-complete - "+"Id:", evt);
                console.log("Id:", id);
                //myFeatureTable.grid.refresh();
            });
        }

        var myWidget = new LayerList({
            map: map,
            layers:[]
        },"layerList");
        myWidget.startup();

        function initializeSidebar(map){
            var popup = map.infoWindow;

            //when the selection changes update the side panel to display the popup info for the
            //currently selected feature.
            connect.connect(popup, "onSelectionChange", function(){

                displayPopupContent(popup.getSelectedFeature());
                //myFeatureTable.clearSelection();
                var query = new Query();
                query.objectIds = [popup.getSelectedFeature().attributes.ObjectID];
                var featureLayer= popup.getSelectedFeature().getLayer();
                featureLayer.selectFeatures(query,FeatureLayer.SELECTION_NEW);
            });

            //when the selection is cleared remove the popup content from the side panel.
            connect.connect(popup, "onClearFeatures", function(){
                //dom.byId replaces dojo.byId
                dom.byId("featureCount").innerHTML = "Click to select feature(s)";
                //registry.byId replaces dijit.byId
                registry.byId("popup1").set("content", "");
                domUtils.hide(dom.byId("pager"));
            });

            //When features are associated with the  map's info window update the sidebar with the new content.
            connect.connect(popup, "onSetFeatures", function(){
                displayPopupContent(popup.getSelectedFeature());
                dom.byId("featureCount").innerHTML = popup.features.length + " feature(s) selected";

                //enable navigation if more than one feature is selected
                popup.features.length > 1 ? domUtils.show(dom.byId("pager")) : domUtils.hide(dom.byId("pager"));
            });
        }
        function displayPopupContent(feature){
            if(feature){
                var content = feature.getContent();
                registry.byId("popup1").set("content", content);
            }
        }

        function selectPrevious(){
            map.infoWindow.selectPrevious();
        }

        function selectNext(){
            map.infoWindow.selectNext();
        }

        function customRefresh(evt){
            //refresh the table
            console.log("custom refresh");
            myFeatureTable.grid.refresh();
        }
        legend.refresh([{layer:'EventosSismicos', title:'Evento Sísmicos'}]);


        var bc = dijit.byId("appLayout");
        var leading = dijit.byId("leftCol");
        var centre = dijit.byId("Central");
        var splitter = bc.getSplitter('left');
        var leadingWidth = dojo.style(leading.domNode, "width");
        on(dom.byId("Boton2"), "click", function(){
                var leadingWidthFinal = dojo.style(leading.domNode, "width");
            if (leadingWidthFinal>0) {
                var slideSplitter = dojo.fx.slideTo({
                    node: splitter.id,
                    duration: 500,
                    left: (dojo.coords(splitter.domNode).l - leadingWidth).toString(),
                    top: dojo.style(splitter.domNode, "top"),
                    unit: "px"
                });
                var slidePane = dojo.animateProperty({
                    node: leading.domNode,
                    duration: 500,
                    properties: {
                        width: {
                            end: dojo.style(leading.domNode, "width") - leadingWidth
                        }
                    }
                });
                var slideCentre = dojo.animateProperty({
                    node: centre.domNode,
                    duration: 500,
                    properties: {
                        width: {
                            end: dojo.style(centre.domNode, "width") + leadingWidth
                        },
                        left: {
                            end: dojo.style(centre.domNode, "left") - leadingWidth
                        }
                    }
                });

                var anim = dojo.fx.combine([slideSplitter, slidePane, slideCentre]);

                dojo.connect(anim, "onEnd", function () {
                    bc.resize();
                });
                anim.play();
            }
            else{
                var slidePane = dojo.animateProperty({
                    node: leading.domNode,
                    duration: 500,
                    properties: {
                        width: {
                            end: dojo.style(leading.domNode, "width") + leadingWidth
                        }
                    },
                    onAnimate: function() {
                        bc.resize();
                    },
                    onEnd: function() {
                        bc.resize();
                    }
                });

                slidePane.play();
            }
            });
        var bcR = dijit.byId("appLayout");
        var leadingR = dijit.byId("RighttCol");
        var centreR = dijit.byId("Central");
        var splitterR = bc.getSplitter('right');
        var leadingWidthR = dojo.style(leading.domNode, "width");
            on(dom.byId("Boton1"), "click", function(){
                //var leadingWidth = dojo.style(leading.domNode, "width");
                var leadingWidthFinalR = dojo.style(leadingR.domNode, "width");
                if (leadingWidthFinalR>0) {
                    var slideSplitterR = dojo.fx.slideTo({
                        node: splitterR.id,
                        duration: 500,
                        left: (dojo.coords(splitterR.domNode).l + leadingWidth).toString(),
                        top: dojo.style(splitterR.domNode, "top"),
                        unit: "px"
                    });
                    var slidePaneR = dojo.animateProperty({
                        node: leadingR.domNode,
                        duration: 500,
                        properties: {
                            width: {
                                end: dojo.style(leadingR.domNode, "width") - leadingWidthR
                            }
                        }
                    });
                    var slideCentreR = dojo.animateProperty({
                        node: centreR.domNode,
                        duration: 500,
                        properties: {
                            width: {
                                end: dojo.style(centreR.domNode, "width") + leadingWidthR
                            },
                            right: {
                                end: dojo.style(centreR.domNode, "right") + leadingWidthR
                            }
                        }
                    });

                    var animR = dojo.fx.combine([slideSplitterR, slidePaneR, slideCentreR]);

                    dojo.connect(animR, "onEnd", function () {
                        bcR.resize();
                    });
                    animR.play();
                }
                else{
                    var slidePaneR = dojo.animateProperty({
                        node: leadingR.domNode,
                        duration: 500,
                        properties: {
                            width: {
                                end: dojo.style(leadingR.domNode, "width") + leadingWidthR
                            }
                        },
                        onAnimate: function() {
                            bcR.resize();
                        },
                        onEnd: function() {
                            bcR.resize();
                        }
                    });

                    slidePaneR.play();
                }

            });


    });

});