var myFeatureTable;
require([
    "esri/layers/FeatureLayer",
    "esri/dijit/FeatureTable",
    "esri/geometry/Extent",
    "esri/graphicsUtils",
    "esri/tasks/query",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/map",
    "dojo/dom",
    "dojo/parser",
    "dojo/ready",
    "dojo/on",
    "dijit/layout/ContentPane",
    "dijit/layout/BorderContainer",
    "esri/Color",
    "esri/dijit/Scalebar",
     "esri/toolbars/navigation",
    "esri/geometry/webMercatorUtils",
    "dojo/_base/lang",
    "esri/dijit/InfoWindowLite",
    "esri/InfoTemplate",
    "dojo/dom-construct",
    "dojo/_base/connect",
    "esri/domUtils",
    "dijit/registry",
    "esri/dijit/Popup",
    "esri/dijit/PopupTemplate",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "dojo/dom-class",
    "esri/dijit/HomeButton",
    "esri/arcgis/utils",
    "esri/dijit/LayerList",
    "esri/request",
], function (
    FeatureLayer, FeatureTable, Extent, graphicsUtils, Query, SimpleMarkerSymbol,SimpleLineSymbol, Map,
    dom, parser, ready, on, ContentPane, BorderContainer , Color,Scalebar, Navigation ,webMercatorUtils, lang, InfoWindowLite, InfoTemplate, domConstruct,connect,domUtils,registry,Popup,PopupTemplate, SimpleFillSymbol, Color,
    domClass,HomeButton,arcgisUtils,LayerList, esri
) {

    //parser.parse();

    ready(function(){
        
        on(dom.byId("previous"), "click", selectPrevious);
        on(dom.byId("next"), "click", selectNext);

        var fill = new SimpleFillSymbol("solid", null, new Color("#A4CE67"));
        var popup = new Popup({
            fillSymbol: fill,
            titleInBody: false
        }, domConstruct.create("div"));
        //Add the dark theme which is customized further in the <style> tag at the top of this page
        domClass.add(popup.domNode, "dark");
        var map = new Map("map",{
            basemap: "topo",
            center: [-72, 4],
            zoom: 12,
            infoWindow: popup
        });
        var home = new HomeButton({
            map: map
        }, "HomeButton");
        home.startup();
        var scalebar = new Scalebar({
            map: map,
            // "dual" displays both miles and kilometers
            // "english" is the default, which displays miles
            // use "metric" for kilometers
            scalebarUnit: "dual"
        });




        //Load a FeatureTable to the application once map loads
        map.on("load", loadTable);

        function loadTable(){


            var censusUrl = "http://srvags.sgc.gov.co/arcgis/rest/services/METG/METADATO/MapServer/0";



            var template = new PopupTemplate({
                title: "Metadato",
                description: "Identificador: {MD_METADATA_ID} <br>Epoca: {EPOCA} <br>Título: {TITLE} <br>Tématica {TEMATICA}<br> <a href={URL}>Detalles</a>  ",
                fieldInfos: [{
                    fieldName: "TITLE",
                    alias:"Título"


                }, {
                    fieldName: "EPOCA",
                    alias:"Epoca"

                }, 
                    {
                    fieldName: "TEMATICA",
                    alias:"Temática"

                },
                    {
                        fieldName: "URL",
                        alias: "Detalles"

                    }]


            });

            var myFeatureLayer = new FeatureLayer(censusUrl, {
                mode: FeatureLayer.MODE_ONDEMAND,
                infoTemplate:template,
                outFields: ["MD_METADATA_ID","TITLE","EPOCA","TEMATICA","URL" ],
                visible: true,
                id: "fLayer"
            });
            on(myFeatureLayer, "load", function(evt){
                var extent = myFeatureLayer.fullExtent;
                //if (webMercatorUtils.canProject(extent, map)) {
                    //map.setExtent( webMercatorUtils.project(extent, map) );
                    on(map, 'extent-change', lang.hitch(this, function(params){
                        var oidFld = myFeatureLayer.objectIdField;
                        var query = new Query();
                        query.geometry = params.extent;
                        query.spatialRelationship = Query.SPATIAL_REL_CONTAINS;
                        myFeatureLayer.queryIds(query, lang.hitch(this, function(objectIds) {
                            myFeatureTable.selectedRowIds = objectIds;
                            myFeatureTable._showSelectedRecords();
                        }));
                    }));
                //}
            });

            //set a selection symbol for the featurelayer.

            //var selectionSymbol = new PictureMarkerSymbol("https://sampleserver6.arcgisonline.com/arcgis/rest/services/RedlandsEmergencyVehicles/FeatureServer/1/images/3540cfc7a09a7bd66f9b7b2114d24eee", 48 ,48);

            //myFeatureLayer.setSelectionSymbol(selectionSymbol);
            var symbol = new SimpleMarkerSymbol(
                SimpleMarkerSymbol.STYLE_CIRCLE,
                12,
                new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_NULL,
                    new Color([247, 34, 101, 0.9]),
                    1
                ),
                new Color([207, 34, 171, 0.5])
            );
            myFeatureLayer.setSelectionSymbol(symbol);

            //Redlands police vehicle locations layer
            // this layer is an editable layer 
            map.addLayer(myFeatureLayer);
            var myWidget = new LayerList({
                map: map,
                layers:[]
            },"layerList");
            myWidget.startup();
            map.infoWindow.set("popupWindow", false);
            initializeSidebar(map);



            //create new FeatureTable and set its properties 
            myFeatureTable = new FeatureTable({
                featureLayer : myFeatureLayer,
                map : map,
                editable: true,
                dateOptions: {
                    datePattern: 'M/d/y',
                    timeEnabled: true,
                    timePattern: 'H:mm',
                },
                //use fieldInfos object to change field's label (column header), 
                //change the editability of the field, and to format how field values are displayed
                //you will not be able to edit callnumber field in this example. 
                fieldInfos: [{
                    name: "TITLE",
                    alias:"Título",
                    visible:false,
                    
                }, {
                    name: "EPOCA",
                    alias:"Epoca",
                    
                }, {
                    name: "TEMATICA",
                    alias:"Temática",
                    
                }],
                //add custom menu functions to the 'Options' drop-down Menu 
                menuFunctions: [
                    {
                        label: "Zoom To Selected Feature(s)",
                        callback: function(evt){
                            console.log(" -- evt: ", evt);
                            var query = new Query();
                            //selectedRowIds property returns ObjectIds of features selected in the feature table
                            //Use the ObjectIds to query the feature layer. In this case, we will zoom to 
                            //selected features on the map
                            query.objectIds = myFeatureTable.selectedRowIds;
                            query.geometry = map.extent;


                            myFeatureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function(features){
                                //zoom to the selected feature
                                //if only one point feature is selected in the table
                                if (features.length == 1 && features[0].geometry.type ==="point") {
                                    maxZoom = map.getMaxZoom();
                                    map.centerAndZoom(features[0].geometry, maxZoom - 1);
                                }
                                else {
                                    var extent = graphicsUtils.graphicsExtent(features);
                                    map.setExtent(extent)
                                }
                                //show only selected features in the feature table. 
                                myFeatureTable.filterSelectedRecords(true);
                            });
                        }
                    },
                    { label: "Custom Refresh",  callback: customRefresh }
                ]
            }, 'myTableNode');




            myFeatureTable.startup();

            myFeatureTable.on("refresh", function(evt){
                console.log("refresh event - ", evt);
            });

            myFeatureTable.on("filter", function(evt){
                console.log("filter event - ", evt);
            });
        }
        function initializeSidebar(map){
            var popup = map.infoWindow;

            //when the selection changes update the side panel to display the popup info for the
            //currently selected feature.
            connect.connect(popup, "onSelectionChange", function(){
                displayPopupContent(popup.getSelectedFeature());
            });

            //when the selection is cleared remove the popup content from the side panel.
            connect.connect(popup, "onClearFeatures", function(){
                //dom.byId replaces dojo.byId
                dom.byId("featureCount").innerHTML = "Click to select feature(s)";
                //registry.byId replaces dijit.byId
                registry.byId("lefpanel").set("content", "");
                domUtils.hide(dom.byId("pager"));
            });

            //When features are associated with the  map's info window update the sidebar with the new content.
            connect.connect(popup, "onSetFeatures", function(){
                displayPopupContent(popup.getSelectedFeature());
                dom.byId("featureCount").innerHTML = popup.features.length + " feature(s) selected";

                //enable navigation if more than one feature is selected
                popup.features.length > 1 ? domUtils.show(dom.byId("pager")) : domUtils.hide(dom.byId("pager"));
            });
        }
        function displayPopupContent(feature){
            if(feature){
                var content = feature.getContent();
                registry.byId("lefpanel").set("content", content);
            }
        }

        function selectPrevious(){
            map.infoWindow.selectPrevious();
        }

        function selectNext(){
            map.infoWindow.selectNext();
        }

        function customRefresh(evt){
            //refresh the table
            console.log("custom refresh");
            myFeatureTable.grid.refresh();
        }
    });
});