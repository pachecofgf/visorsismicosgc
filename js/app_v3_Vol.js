var myFeatureTable;
require([
    "esri/layers/FeatureLayer",
    "esri/dijit/FeatureTable",
    "esri/geometry/Extent",
    "esri/graphicsUtils",
    "esri/tasks/query",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/map",
    "dojo/dom",
    "dojo/parser",
    "dojo/ready",
    "dojo/on",
    "dijit/layout/ContentPane",
    "dijit/layout/BorderContainer",
    "esri/Color",
    "esri/dijit/Scalebar",
    "esri/toolbars/navigation",
    "esri/geometry/webMercatorUtils",
    "dojo/_base/lang",
    "esri/dijit/InfoWindowLite",
    "esri/InfoTemplate",
    "dojo/dom-construct",
    "dojo/_base/connect",
    "esri/domUtils",
    "dijit/registry",
    "esri/dijit/Popup",
    "esri/dijit/PopupTemplate",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "dojo/dom-class",
    "esri/dijit/HomeButton",
    "esri/arcgis/utils",
    "esri/dijit/LayerList",
    "esri/request",
    "esri/geometry/Point",
    "esri/graphic",
    "esri/tasks/BufferParameters",
    "esri/tasks/GeometryService",
    "dijit/form/RadioButton",
    "dijit/form/Button",
    "dojo/request",
    "esri/dijit/Legend",
    "esri/renderers/ClassBreaksRenderer",
    "dojo/fx/Toggler",
    "dojo/fx",
    "dojo/dom-geometry",
    "esri/dijit/BasemapGallery",
    "esri/dijit/OverviewMap",
    "dojo/_base/xhr",
    "esri/tasks/FeatureSet",
    "dojo/_base/array",
    'dojox/grid/DataGrid',
    'dojo/data/ItemFileWriteStore',
    "dijit/Tooltip",
], function (
    FeatureLayer, FeatureTable, Extent, graphicsUtils, Query, SimpleMarkerSymbol,SimpleLineSymbol, Map,
    dom, parser, ready, on, ContentPane, BorderContainer ,Color, Scalebar, Navigation ,webMercatorUtils, lang, InfoWindowLite, InfoTemplate, domConstruct,connect,domUtils,registry,Popup,PopupTemplate, SimpleFillSymbol, Color,
    domClass,HomeButton,arcgisUtils,LayerList,esriRequest,Point,Graphic,BufferParameters,GeometryService,RadioButton,Button,request,Legend,ClassBreaksRenderer,Toggler,coreFx,domGeom,BasemapGallery,OverviewMap,xhr,FeatureSet,array,
    DataGrid, ItemFileWriteStore,Tooltip
) {

    //parser.parse();
    esriConfig.defaults.io.corsEnabledServers.push('https://srvags.sgc.gov.co/VolcanesSGCJson/Volcanes/Volcanes.json');
    
    var url_volcanes = "https://srvags.sgc.gov.co/VolcanesSGCJson/Volcanes/Volcanes.json";
    //var url_volcanes = "js/Volcanes.json";
    //esriConfig.defaults.io.proxyUrl =

    ready(function(){
        //parser.parse();

        var fill = new SimpleFillSymbol("solid", null, new Color("#A4CE67"));
        var popup = new Popup({
            fillSymbol: fill,
            titleInBody: false
        }, domConstruct.create("div"));
        //Add the dark theme which is customized further in the <style> tag at the top of this page
        domClass.add(popup.domNode, "dark");

        var map = new Map("map",{
            basemap: "topo",
            center: [-72, 4],
            zoom: 6,
            infoWindow: popup
        });
        /*
        var globals;
        globals = new OverviewMap({
            map: map,
            visible: true
        }, dom.byId('overviewMapDiv'));
        globals.startup();
        */
        var basemapGallery = new BasemapGallery({
            showArcGISBasemaps: true,
            map: map
        }, "basemapGallery");
        basemapGallery.startup();

        basemapGallery.on("error", function(msg) {
            console.log("basemap gallery error:  ", msg);
        });

        /*
        var home = new HomeButton({
            map: map
        }, "HomeButton");
        home.startup();
      */
        var scalebar = new Scalebar({
            map: map,
            // "dual" displays both miles and kilometers
            // "english" is the default, which displays miles
            // use "metric" for kilometers
            scalebarUnit: "dual"
        });

        map.on("load", loadTable);
        /*
        var legend = new Legend({
            map: map
        }, "legendDiv");
        legend.startup();
        */

        function loadTable() {

            var featureCollection = {
                "layerDefinition": null,
                "featureSet": {
                    "features": [],
                    "geometryType": "esriGeometryPoint"
                }
            };
            featureCollection.layerDefinition = {
                "geometryType": "esriGeometryPoint",
                "objectIdField": "ObjectID",

                "displayFieldName": "",
                "drawingInfo": {
                    "renderer": {
                        "type": "uniqueValue",
                        "field1": "ACTIVIDAD",
                        "field2": null,
                        "field3": null,
                        "fieldDelimiter": ", ",
                        "defaultSymbol": null,
                        "defaultLabel": null,
                        "uniqueValueInfos": [
                            {
                                "symbol": {
                                    "type": "esriPMS",
                                    "url": "5dae362f54c9094f531b26b02039e5f6",
                                    "imageData": "iVBORw0KGgoAAAANSUhEUgAAABcAAAAUCAYAAABmvqYOAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAXxJREFUOI211L9rFEEYBuAnepepEuQQ4URQMCKKhRiwM0mTKoiIXLQTFY1FLBQULDwlGhMMBIQU/ipsrOwU/SPs7WysthC2FL5CsNg7OdbT3B2bFxaW2eEZFr55a3YwtSH3L+DTTuAX8RqH8aNKPGEdE2jjVpX4bRzqvC/hOb5Vge/DfVjFEeqLPMViFfhjTCZcwSTO0PraaJzO8/zLyHiz2TyRZdk1eINmZ72N+TzfwOzIeJZlm9g9jXM96zNoMfOes/g4Cr6AeVhTjEk347iLDymtR8Rn/BoGr6WUNiLCJcz12TCN6xHHt7iqmP+B8ZsRcQzuod5nwy4sY4tHeIefg+B78JBi/k7+43Q4ihX2t7mDJ9viKaUHEbGX4raM/QenGM928YMvlWqhjE9FxDK8wMFtYDiAV0zc6FMLZfwZxqcMcP16cgGrLH0v1cIfPKU0GxHndb42hsA7qSvVQhcfi4jN4b2/0mr01EIXv4xTFeDynlro4m87T6X5DQiHXLAt1uR4AAAAAElFTkSuQmCC",
                                    "contentType": "image/png",
                                    "width": 17,
                                    "height": 15,
                                    "angle": 0,
                                    "xoffset": 0,
                                    "yoffset": 0
                                },
                                "value": "I",
                                "label": "I",
                                "description": ""
                            },
                            {
                                "symbol": {
                                    "type": "esriPMS",
                                    "url": "9274b990609ed1781caa2528bbd534f1",
                                    "imageData": "iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAVFJREFUSIm91qFLBEEUx/Gv68kTVjhZLZoMBv8BEeMFvf9Ao1y4C/4PggZFLAbBdGATDSYxCIJFw1aLzaoYJl15zbDcebu3tztv5fyV4b2Zx4eBCVPjn1KrMBMA84CbJBQAXRHZVNUG8DUJKAC6QEtVEZFnC+YLDZB+Q1XXLJgPlEJ2NmBlEc4ebFgZNIJcdiAUqAVwcu+PFUG5yMJcsnmwm6y+2DioEAGYnbFheVApUgXLQt6IFRuGzIgFG0Aicq6qLYAlgYu2H5LFPr7hJk5h60BvAIVheK2qe0D9U+HuFTpNmJ7yx17eE2Qot0AvdSPnXBxFUdM59wjU96+Svi/29AZbx7+1iByp6mG/Tj2GqlgZMgJVwXyQXMiC+SJjIR/MghRCRdjqMmwbkFKoCLMgXlAeZkW8oTzMgpigDNZQ1VPLrPm75ZyLgbj04F+hqvkBIVEb08dKBFAAAAAASUVORK5CYII=",
                                    "contentType": "image/png",
                                    "width": 19,
                                    "height": 19,
                                    "angle": 0,
                                    "xoffset": 0,
                                    "yoffset": 0
                                },
                                "value": "II",
                                "label": "II",
                                "description": ""
                            },
                            {
                                "symbol": {
                                    "type": "esriPMS",
                                    "url": "33143286bbb5c14d4ee6b060f02c46f5",
                                    "imageData": "iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAHNJREFUOI1jYaASYIHStQwMDOrkGMDOzv7658+fhTCD3BkYGKzJMejnz58PGRgY4AYxMDAwMNy/n8IgJMROlAFubssZTp58B+ejGMTHx8bAx8dJlEFsbKh8FuzKSAejBo0aRHWDhIWnUccgSgDVDbKh1CAAIb0RU2r6TNgAAAAASUVORK5CYII=",
                                    "contentType": "image/png",
                                    "width": 13,
                                    "height": 13,
                                    "angle": 0,
                                    "xoffset": 0,
                                    "yoffset": 0
                                },
                                "value": "III",
                                "label": "III",
                                "description": ""
                            },
                            {
                                "symbol": {
                                    "type": "esriPMS",
                                    "url": "1ad1b1ebbea0d2de5d6620f909488ec5",
                                    "imageData": "iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAc5JREFUOI2t0j9oU1EUx/Fv4JmTSjBwwUHov6BYHXWo4iToIBVEqFgsbo6CLjq42cVJRMgiLnVIcSoqugi1iJSCQkeFihA1mA6F49KCv9pUhzSv7zV/zOCZ3rmX+znvnHsj/lNEnTZCCCfd/SwwDGSAb2Y2J2mxJyiEcNzdS+5+aveepCngQwjhhru/7waNu3uZIXKHxiFfBPqADVj7AdXnoI+Muvs74BpQboFCCKPuPhNOY4NXwfLpChagMAKVF+CzZIFpM6tJmk9CGXd/xAFscLIViatmoXgR1ldAi0SSHgNHgd9N6BxwbOgy2L72SIztgeExWG6M/CAwAZSb0Bhsz6SHyPen0vNJaAQgt7c3KMoBBgiAw7AzoyxAXV0eViI2N2IkPts8V4PGFdv+f0NrK6m0loQWgCvVZ1A4sv3rnWILVudTKwtJ6ClwT8sUKrNQvASRtUHq8H0O/HW88gt4koR+AlPAA38J6zUYuAD5AbA+2FSjndW3KQQzuy+pmoQAHgIngAktwZelLu014pWku80kCf0BJs2sIukWnS+wbmYlSbcbzbZCAFuS7gDTwE0zOyOpCGTM7KukN0BJ0qfdeqeqn4Hr0s5jSX63i7/QsqRir1vlpQAAAABJRU5ErkJggg==",
                                    "contentType": "image/png",
                                    "width": 13,
                                    "height": 13,
                                    "angle": 0,
                                    "xoffset": 0,
                                    "yoffset": 0
                                },
                                "value": "IV",
                                "label": "IV",
                                "description": ""
                            }
                        ]
                    },
                    "transparency": 0,
                    "labelingInfo": null
                },
                "spatialReference": {
                    "wkid": 4686,
                    "latestWkid": 4686
                },
                "fields": [
                    {
                        "name": "ObjectID",
                        "type": "esriFieldTypeOID",
                        "alias": "ObjectID",
                    },
                    {
                        "name": "Shape",
                        "type": "esriFieldTypeGeometry",
                        "alias": "Shape",
                    },
                    {
                        "name": "NOMBREVOLC",
                        "type": "esriFieldTypeString",
                        "alias": "NOMBREVOLC",
                        "length": 50,
                    },
                    {
                        "name": "ALTURASOBR",
                        "type": "esriFieldTypeDouble",
                        "alias": "ALTURASOBR",
                    },
                    {
                        "name": "LATITUD",
                        "type": "esriFieldTypeString",
                        "alias": "LATITUD",
                        "length": 25,
                    },
                    {
                        "name": "LONGITUD",
                        "type": "esriFieldTypeString",
                        "alias": "LONGITUD",
                        "length": 25,
                    },
                    {
                        "name": "URL",
                        "type": "esriFieldTypeString",
                        "alias": "URL",
                        "length": 254,
                    },
                    {
                        "name": "ACTIVIDAD",
                        "type": "esriFieldTypeString",
                        "alias": "ACTIVIDAD",
                        "length": 20,
                    },
                    {
                        "name": "IMAGEN",
                        "type": "esriFieldTypeString",
                        "alias": "IMAGEN",
                        "length": 254,
                    },
                    {
                        "name": "DETALLE",
                        "type": "esriFieldTypeString",
                        "alias": "DETALLE",
                        "length": 254,
                    }
                ],

            };
            var template = new PopupTemplate({
                title: "{NOMBREVOLC}",
                description: "Altura Sobre el Nivel del Mar: {ALTURASOBR} <br>" +
                "Latitud: {LATITUD} <br> Longitud: {LONGITUD} <br> Actividad: <img style='vertical-align:middle;' src='css/images/Volcanes/Volcanes_" + "{ACTIVIDAD}" + ".png' height='40' width='40'> <br> <a href={DETALLE}> PAGINA DETALLES </a>"
                ,
                mediaInfos: [
                    {
                    "title": "",
                    "caption": "",
                    "type": "image",
                    "value": {
                        "sourceURL": "{IMAGEN}",
                        "linkURL": "{DETALLE}"
                    }
                }]

            });
            var myFeatureLayer = new FeatureLayer(featureCollection, {
                id: 'volcanes',
                mode: FeatureLayer.MODE_ONDEMAND,
                outFields: ["*"],
                infoTemplate: template
            });

            function requestPhotos() {
                //get geotagged photos from flickr
                //tags=flower&tagmode=all
                var requestHandle = esriRequest({
                    url: url_volcanes,
                    callbackParamName: "jsoncallback"
                });
                requestHandle.then(requestSucceeded, requestFailed);
            }

            function requestSucceeded(response, io) {
                //loop through the items and add to the feature layer

                console.log("Respuesta exitosa");
                var features = [];

                var data = {
                    identifier: "id",
                    items: []
                };
                var data_list = [];
                array.forEach(response.features, function (item) {
                    var attr = {};

                    //pull in any additional attributes if required
                    //attr["ObjectID"] = item.attributes.ObjectID;
                    attr["NOMBREVOLC"] = item.attributes.NOMBREVOLC;

                    attr["ALTURASOBR"] = item.attributes.ALTURASOBR;
                    attr["LATITUD"] = item.attributes.LATITUD;
                    attr["LONGITUD"] = item.attributes.LONGITUD;
                    attr["URL"] = item.attributes.URL;
                    attr["ACTIVIDAD"] = item.attributes.ACTIVIDAD;
                    attr["IMAGEN"] = item.attributes.IMAGEN;
                    attr["DETALLE"] = item.attributes.DETALLE;
                    var geometry = new Point(item.geometry.x, item.geometry.y);
                    var graphic = new Graphic(geometry);
                    graphic.setAttributes(attr);
                    features.push(graphic);
                    //attr["ObjectID"] = features[0];
                    data_list.push(attr);

                });
                for (i = 0; i < data_list.length; i++) {
                    data.items.push(lang.mixin({id: i},data_list[i]));

                }
                var store = new ItemFileWriteStore({data: data});
                var layout = [[
                    {'name': 'Identificador', 'field': 'id', 'width': '0px'},
                    {'name': 'NOMBREVOLC', 'field': 'NOMBREVOLC', 'width': '100%'}

                ]];
                var grid = new DataGrid({
                    id: 'grid',
                    store: store,
                    structure: layout,
                    rowSelector: '10px'});
                grid.placeAt("myTableNode");
                grid.layout.setColumnVisibility(0,false);
                grid.startup();
                myFeatureLayer.applyEdits(features, null, null);
                dojo.connect(grid, "onRowClick", function(e) {
                    myFeatureLayer.clearSelection();
                    var items = grid.selection.getSelected();
                    if(items.length) {
                        array.forEach(items, function(selectedItem){
                            if(selectedItem !== null){
                                array.forEach(grid.store.getAttributes(selectedItem), function(attribute){
                                    var value = grid.store.getValues(selectedItem, attribute);
                                    if (attribute=="id")
                                    {
                                        var query = new Query();
                                        //selectedRowIds property returns ObjectIds of features selected in the feature table
                                        //Use the ObjectIds to query the feature layer. In this case, we will zoom to
                                        //selected features on the map
                                        query.objectIds = value;
                                        //query.geometry = map.extent;


                                        myFeatureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function (features) {
                                            //zoom to the selected feature
                                            //if only one point feature is selected in the table
                                            if (features.length == 1 && features[0].geometry.type == "point")
                                            {
                                                popup.clearFeatures();
                                                maxZoom = map.getMaxZoom();
                                                map.centerAndZoom(features[0].geometry, maxZoom - 8);
                                                popup=map.infoWindow;
                                                var content = features[0].getContent();
                                                popup.setContent(content);
                                                popup.show(features[0].geometry);

                                            }
                                            else
                                            {
                                                var extent = graphicsUtils.graphicsExtent(features);
                                                map.setExtent(extent);
                                            }
                                        });
                                        console.dir('attribute: ' + attribute + ', value: ' + value);
                                    }
                                });
                            }
                        });
                    }

                });


            }



            function requestFailed(error) {
                console.log('failed');
            }

            requestPhotos();
            map.addLayer(myFeatureLayer);


            var myWidget = new LayerList({
                map: map,
                layers: [{id:"volcanes",layer:myFeatureLayer}]
            }, "layerList");
            myWidget.startup();


            function toggleSidebar(appLayout, contentAccordion) {
                var panelIndex = appLayout.getIndexOfChild(contentAccordion);
                if(panelIndex >= 0) {
                    appLayout.removeChild(contentAccordion);
                } else {
                    appLayout.addChild(contentAccordion);
                }
            }

            var bc = dijit.byId("appLayout");
            var leadingR = dijit.byId("RighttCol");
            var leadingL = dijit.byId("leftCol");

            on(dom.byId("Boton2"), "click", function () {
                toggleSidebar(bc, leadingL);
            });

            on(dom.byId("Boton1"), "click", function () {
                toggleSidebar(bc, leadingR);
            });

            on(dom.byId("Boton4"), "click", function () {
                var schoolGraphicsExtent = graphicsUtils.graphicsExtent(myFeatureLayer.graphics);
                map.setExtent(schoolGraphicsExtent, true);
            });

            new Tooltip({
                connectId: ["IV"],
                label: "Volcán activo y comportamiento estable"
            });
            new Tooltip({
                connectId: ["III"],
                label: "Cambios en el comportamiento de la actividad volcánica"
            });
            new Tooltip({
                connectId: ["II"],
                label: "Erupción probable en termino de dias o semanas"
            });
            new Tooltip({
                connectId: ["I"],
                label: "Erupción inminente o en curso"
            });

            

            on(dom.byId("Leyenda"), "click", function () {
                window.open("https://www2.sgc.gov.co/Paginas/niveles-actividad-volcanes.aspx")
            });


        }
    });

});