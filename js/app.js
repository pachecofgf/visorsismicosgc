require([
    "esri/map",
    "esri/dijit/Popup",
    "esri/dijit/PopupTemplate",
    "esri/layers/FeatureLayer",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojox/charting/Chart",
    "dojox/charting/themes/Dollar",
    "dojo/domReady!"
], function(
    Map,
    Popup,
    PopupTemplate,
    FeatureLayer,
    SimpleFillSymbol,
    Color,
    domClass,
    domConstruct,
    on,
    Chart,
    theme,
    ready
) {

    var fill = new SimpleFillSymbol("solid", null, new Color("#A4CE67"));
    var popup = new Popup({
        fillSymbol: fill,
        titleInBody: false
    }, domConstruct.create("div"));
    //Add the dark theme which is customized further in the <style> tag at the top of this page
    domClass.add(popup.domNode, "white");
        var censusUrl = "http://srvags.sgc.gov.co/arcgis/rest/services/METG/METADATO/MapServer/0",
        map = new Map("map", {
            basemap: "topo",
            center: [-72, 4],
            zoom: 6,
            infoWindow: popup
        });
    //Template de la ventana emergente
    var template = new PopupTemplate({
        title: "Identificador Metadato {MD_METADATA_ID}",
        fieldInfos: [{
            fieldName: "TITLE",
            alias:"Título",
            visible: true,
            format: {
                places: 0
            }
        }, {
            fieldName: "EPOCA",
            alias:"Epoca",
            visible: true,
            format: {
                places: 0
            }
        }, {
            fieldName: "TEMATICA",
            alias:"Temática",
            visible: true,
            format: {
                places: 0
            }
        }]
    });

    var featureLayer = new FeatureLayer(censusUrl,{
        mode: FeatureLayer.MODE_SNAPSHOT,
        outFields: ["*"],
        infoTemplate: template
    });




    

    map.addLayer(featureLayer);




});
